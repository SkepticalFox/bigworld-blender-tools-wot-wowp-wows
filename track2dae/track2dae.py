''' SkepticalFox 2015-2024 '''



import argparse
from XmlUnpacker import XmlUnpacker



def prettify(elem):
	from xml.etree import ElementTree as ET
	from xml.dom import minidom
	reparsed = minidom.parseString(ET.tostring(elem))
	return reparsed.toprettyxml()



parser = argparse.ArgumentParser()
parser.add_argument('filename')
args = parser.parse_args()

track_filename = args.filename
dae_filename = track_filename.replace('.track', '.dae')

with open(track_filename, 'rb') as file:
	xmlr = XmlUnpacker()
	with open(dae_filename, 'w') as fw:
		fw.write(prettify(xmlr.read(file)))
