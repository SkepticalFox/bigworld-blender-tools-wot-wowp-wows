''' SkepticalFox 2015-2024 '''



#####################################################################
# Addon - header

bl_info = {
    "name": "World of Warplanes - Airplane Viewer",
    "author": "SkepticalFox",
    "version": (0, 0, 2),
    "blender": (4, 3, 0),
    "location": "View3D > Tool Shelf > AirplaneViewer",
    "description": "WoWP Airplane Viewer",
    "warning": "Test version",
    "wiki_url": "https://www.koreanrandom.com/forum/topic/28240-/",
    "category": "3D View",
}



#####################################################################
# Import settings

from .settings import *



#####################################################################
# imports

import os

from .PackageManager import *
from .AircraftUtils import *
from .BigWorldModelLoader import *
from .common import *

import bpy



#####################################################################
# defaults

wowp_parse_status = False
wowp_version = None
current_wowp_models = {}

user_preferences = bpy.context.user_preferences



#####################################################################
# Delete models

def deleteModels(kill_dict):
    use_global_undo_orig = user_preferences.edit.use_global_undo
    user_preferences.edit.use_global_undo = False

    if kill_dict.get('Objects'):
        for object_name in kill_dict['Objects']:
            if bpy.data.objects.get(object_name):
                object_ = bpy.data.objects[object_name]
                bpy.context.scene.objects.unlink(object_)
                bpy.data.objects.remove(object_)

    if kill_dict.get('Meshes'):
        for mesh_name in kill_dict['Meshes']:
            if bpy.data.meshes.get(mesh_name):
                mesh_ = bpy.data.meshes[mesh_name]
                bpy.data.meshes.remove(mesh_)

    if kill_dict.get('Materials'):
        for material_name in kill_dict['Materials']:
            if bpy.data.materials.get(material_name):
                material_ = bpy.data.materials[material_name]
                bpy.data.materials.remove(material_)

    if kill_dict.get('Textures'):
        for texture_name in kill_dict['Textures']:
            if bpy.data.textures.get(texture_name):
                texture_ = bpy.data.textures[texture_name]
                bpy.data.textures.remove(texture_)

    if kill_dict.get('Images'):
        for image_name in kill_dict['Images']:
            if bpy.data.images.get(image_name):
                image_ = bpy.data.images[image_name]
                bpy.data.images.remove(image_)

    if kill_dict.get('Curves'):
        for curve_name in kill_dict['Curves']:
            if bpy.data.curves.get(curve_name):
                curve_ = bpy.data.curves[curve_name]
                if curve_.users>0:
                    try:
                        curve_.user_clear()
                    except:pass
                bpy.data.curves.remove(curve_)

    kill_dict.clear()
    kill_dict['Meshes'] = []
    kill_dict['Curves'] = []
    kill_dict['Objects'] = []
    kill_dict['Images'] = []
    kill_dict['Materials'] = []
    kill_dict['Textures'] = []

    user_preferences.edit.use_global_undo = use_global_undo_orig



#####################################################################
# loadByAircraftInfo

def loadByAircraftInfo(scene):
    global current_tank_info, current_wowp_models
    deleteModels(current_wowp_models)
    custom = scene.custom_wowp[scene.custom_index_wowp]
    current_airplane_xml_file = custom.xml_file
    info = g_AircraftUtils.loadInfo(current_airplane_xml_file, custom.nation_id)
    value = info['hull']
    value['is_root'] = True
    _dict_positions = g_BigWorldModelLoader.load(value, current_wowp_models)
    for key, value in info.items():
        if key == 'hull':
            continue
        value['is_child'] = True
        if value['mountPoint'] in _dict_positions:
            value['Position'] = _dict_positions[value['mountPoint']]
        g_BigWorldModelLoader.load(value, current_wowp_models)



#####################################################################
# get version

def get_version(filename):
    from xml.etree.ElementTree import parse
    tree = parse(filename)
    elem = tree.getroot()
    # Example: <build>1.9.3</build>
    _version = elem.find('build').text.strip()
    print('Info: WoWP client: %s' % _version)
    return tuple(map(int, _version.split('.')))



#####################################################################
# Apply Path Operator

class Apply_Path_Operator(bpy.types.Operator):
    bl_label = 'Operator apply paths'
    bl_idname = 'warplane.apply_path_op'

    def execute(self, context):
        global wowp_parse_status, wowp_version

        if g_PackageManager.is_loaded == False:
            prefs = user_preferences.addons[__package__].preferences
            wowp_exe_path = os.path.join(prefs.world_of_warplanes_game_path, 'WorldOfWarplanes.exe')
            wowp_ver_path = os.path.join(prefs.world_of_warplanes_game_path, 'res\\build.xml')

            if os.path.exists(wowp_exe_path) and os.path.exists(wowp_ver_path):
                wowp_version = get_version(wowp_ver_path)

                res_pkg_path = os.path.join(prefs.world_of_warplanes_game_path, 'res\\packages')
                g_PackageManager.load(res_pkg_path)

                g_AircraftUtils.setPath(os.path.join(prefs.world_of_warplanes_game_path, 'res'))
                g_AircraftUtils.loadTranslations()
                g_AircraftUtils.loadAircraftTable()

                nation_l10n_items = []
                for idx, nation_l10n in g_NationsL10n.INDEX.items():
                    nation_l10n_items.append((str(idx), nation_l10n, ''))
                nation_l10n_items.sort()

                category_l10n_items = []
                for idx, category_l10n in g_CategoryL10n.INDEX.items():
                    category_l10n_items.insert(idx, (str(idx), category_l10n, ''))

                level_l10n_items = []
                for idx, level_l10n in g_LevelsL10n.INDEX.items():
                    level_l10n_items.insert(idx, (str(idx), level_l10n, ''))

                # register
                bpy.utils.register_class(Apply_Filter_Operator)
                bpy.utils.register_class(UI_Aircraft_List)
                bpy.utils.register_class(AirplaneCustomProp)
                bpy.types.Scene.custom_wowp = bpy.props.CollectionProperty(type=AirplaneCustomProp)
                bpy.types.Scene.custom_index_wowp = bpy.props.IntProperty()

                bpy.types.Scene.nations_l10n_wowp = bpy.props.EnumProperty(
                    name = 'Nations',
                    description = '',
                    items = nation_l10n_items,
                )

                bpy.types.Scene.category_l10n_wowp = bpy.props.EnumProperty(
                    name = 'Category',
                    description = '',
                    items = category_l10n_items,
                )

                bpy.types.Scene.levels_l10n_wowp = bpy.props.EnumProperty(
                    name = 'Tier',
                    description = '',
                    items = level_l10n_items,
                )

                bpy.utils.register_class(Panel_Airplane_Filter)
                bpy.utils.register_class(Panel_Airplane_List)
                bpy.utils.register_class(Apply_Airplane_Operator)

                # unregister
                bpy.utils.unregister_class(Panel_AirplaneViewer_Paths)
                bpy.utils.unregister_class(Apply_Path_Operator)

                wowp_parse_status = True
            else:
                # INFO, WARNING or ERROR
                self.report({'WARNING'}, 'Error in path!')
        return {'FINISHED'}



#####################################################################
# Panel AirplaneViewer Paths

class Panel_AirplaneViewer_Paths(bpy.types.Panel):
    bl_label = 'Paths'
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = 'AirplaneViewer'

    def draw(self, context):
        layout = self.layout

        layout.operator('warplane.apply_path_op', text='Parse WoWP')



#####################################################################
# Apply Filter Operator

class Apply_Filter_Operator(bpy.types.Operator):
    bl_label = 'Operator apply filter'
    bl_idname = 'warplane.apply_filter'

    def execute(self, context):
        scn = context.scene
        custom = scn.custom_wowp
        scn.custom_index_wowp = 0
        custom.clear()

        if wowp_parse_status:
            level = scn.levels_l10n_wowp
            category = scn.category_l10n_wowp
            nation = scn.nations_l10n_wowp

            level = '=%s' % level if ( level != '0' ) else ''
            category = '=%s' % category if ( category != '0' ) else ''
            nation = '=%s' % nation if ( nation != '0' ) else ''

            for row in g_AircraftUtils.sqlite3_cursor.execute('SELECT nation_id, name, name_in_res FROM AircraftList_table WHERE nation_id%s AND type_id%s AND level%s;' % (nation, category, level)):
                aircraft_list_item = custom.add()
                aircraft_list_item.nation_id = row[0]
                aircraft_list_item.name = row[1]
                aircraft_list_item.xml_file = row[2]

        return {'FINISHED'}



#####################################################################
# Airplane Filter Panel

class Panel_Airplane_Filter(bpy.types.Panel):
    bl_label = 'Airplane Filter'
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = 'AirplaneViewer'

    def draw(self, context):
        layout = self.layout

        scn = context.scene

        box = layout.row().box()
        box.prop(scn, 'nations_l10n_wowp', text='')
        box.prop(scn, 'category_l10n_wowp', text='')
        box.prop(scn, 'levels_l10n_wowp', text='')

        layout.separator()
        layout.operator('warplane.apply_filter', text='Apply Filter')



#####################################################################
# Apply Airplane Operator

class Apply_Airplane_Operator(bpy.types.Operator):
    bl_label = 'Operator apply airplane'
    bl_idname = 'av.apply_airplane'

    @classmethod
    def poll(self, context):
        if len(context.scene.custom_wowp):
            custom = context.scene.custom_wowp[context.scene.custom_index_wowp]
            if (context.active_object is None) or (context.active_object.mode == 'OBJECT'):
                return True
        return False

    def execute(self, context):
        scn = context.scene
        current_airplane_name = scn.custom_wowp[scn.custom_index_wowp].name
        av_PrintSplitter()
        av_PrintInfo('airplane name: `%s`' % current_airplane_name)
        try:
            loadByAircraftInfo(scn)
        except:
            self.report({'ERROR'}, 'Error in load `%s`!' % current_airplane_name)
            import traceback
            traceback.print_exc()
        return {'FINISHED'}



#####################################################################
# AirplaneCustomProp

class AirplaneCustomProp(bpy.types.PropertyGroup):
    name = bpy.props.StringProperty()
    xml_file = bpy.props.StringProperty()
    nation_id = bpy.props.IntProperty()



#####################################################################
# UI_Aircraft_List

class UI_Aircraft_List(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        layout.label(text=item.name, translate=False)



#####################################################################
# Airplane List Panel

class Panel_Airplane_List(bpy.types.Panel):
    bl_label = 'Airplane List'
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_category = 'AirplaneViewer'

    def draw(self, context):
        layout = self.layout
        scn = context.scene
        layout.row().template_list('UI_Aircraft_List', '', scn, 'custom_wowp', scn, 'custom_index_wowp')

        layout.separator()
        layout.operator('av.apply_airplane', text='Load Airplane')



#####################################################################
# WarplaneViewerPreferences

class WarplaneViewerPreferences(bpy.types.AddonPreferences):
    bl_idname = __package__

    world_of_warplanes_game_path = bpy.props.StringProperty(
        name = 'WoWP Path',
        subtype = 'DIR_PATH',
        default = WOWP_PATH_DEFAULT
    )

    world_of_warplanes_custom_path = bpy.props.StringProperty(
        name = 'Custom Path',
        subtype = 'DIR_PATH',
        default = WOWP_CUSTOM_PATH_DEFAULT
    )

    def draw(self, context):
        layout = self.layout

        layout.prop(self, 'world_of_warplanes_game_path')
        layout.prop(self, 'world_of_warplanes_custom_path')



#####################################################################
# unregister

def unregister():
    global wowp_parse_status

    bpy.utils.unregister_class(WarplaneViewerPreferences)

    if wowp_parse_status:
        wowp_parse_status = False
        g_PackageManager.clear()

        bpy.utils.unregister_class(UI_Aircraft_List)
        bpy.utils.unregister_class(AirplaneCustomProp)
        bpy.utils.unregister_class(Apply_Filter_Operator)
        bpy.utils.unregister_class(Panel_Airplane_Filter)
        bpy.utils.unregister_class(Panel_Airplane_List)
        bpy.utils.unregister_class(Apply_Airplane_Operator)

    else:
        bpy.utils.unregister_class(Panel_AirplaneViewer_Paths)
        bpy.utils.unregister_class(Apply_Path_Operator)



#####################################################################
# register

def register():
    bpy.utils.register_class(WarplaneViewerPreferences)

    bpy.utils.register_class(Panel_AirplaneViewer_Paths)
    bpy.utils.register_class(Apply_Path_Operator)
