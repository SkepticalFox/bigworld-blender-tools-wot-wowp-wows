''' SkepticalFox 2015-2024 '''



__all__ = ('g_BigWorldModelLoader',)



#####################################################################
# imports

import os

from .LoadDataMesh import LoadDataMesh
# from .BigWorldMaterial import *
from .PackageManager import *
from .common import *

import bpy

from mathutils import Vector
from bpy_extras.io_utils import unpack_list
# from bpy_extras.image_utils import load_image



#####################################################################
# functions

def remove_unp_file(unp_filepath):
    try:
        os.remove(unp_filepath)
    except:
        import traceback
        traceback.print_exc()



#####################################################################
# BigWorldModelLoader

class BigWorldModelLoader:
    def load_positions(self, visual):
        _dict = {}
        for node in visual.findall('node/node/node/node'):
            _dict[node.find('identifier').text.strip()] = av_AsVector(node.find('transform/row3').text.strip()).xzy
        return _dict

    def load(self, model, kill_list):
        model_name = os.path.splitext(os.path.basename(model['File']))[0]
        av_PrintInfo('Start loading model: `%s`' % model['File'])
        unp_model_filepath = g_PackageManager.extract_file(model['File'])

        if not unp_model_filepath:
            return

        with open(unp_model_filepath, 'rb') as f:
            model_xml = g_XmlUnpacker.read(f)

        remove_unp_file(unp_model_filepath)

        if model_xml.find('nodefullVisual') is not None:
            visual_filepath = av_AsNormPath(model_xml.find('nodefullVisual').text)
            visual_filepath = '%s.visual' % visual_filepath
        elif model_xml.find('nodelessVisual') is not None:
            visual_filepath = av_AsNormPath(model_xml.find('nodelessVisual').text)
            visual_filepath = '%s.visual' % visual_filepath
        else:
            av_PrintWarn('`nodefullVisual` and `nodelessVisual` not in .model')
            visual_filepath = '%s.visual' % os.path.splitext(model['File'])[0]

        del model_xml

        unp_visual_filepath = g_PackageManager.extract_file(visual_filepath)

        if not unp_visual_filepath:
            av_PrintError('Error in visual unpacking')
            return

        with open(unp_visual_filepath, 'rb') as f:
            visual_xml = g_XmlUnpacker.read(f)

        remove_unp_file(unp_visual_filepath)

        primitives_filepath = visual_filepath.replace('.visual', '.primitives')

        unp_primitives_filepath = g_PackageManager.extract_file(primitives_filepath)

        if visual_xml.find('renderSet') is None:
            av_PrintWarn('renderSet is None')
            remove_unp_file(unp_primitives_filepath)
            return

        for renderSet in visual_xml.findall('renderSet'):
            vres_name = renderSet.find('geometry/vertices').text.strip()
            pres_name = renderSet.find('geometry/primitive').text.strip()

            if len(vres_name.split('.'))>1:
                mesh_name = bpy.path.clean_name(os.path.splitext(vres_name)[0])
            else:
                mesh_name = bpy.path.clean_name(model_name)

            bmesh = bpy.data.meshes.new(mesh_name)
            kill_list['Meshes'].append(bmesh.name)

            uv2_name = ''
            if renderSet.find('geometry/stream') is not None:
                stream_res_name = renderSet.find('geometry/stream').text.strip()
                if 'uv2' in stream_res_name:
                    uv2_name = stream_res_name

            dataMesh = LoadDataMesh(unp_primitives_filepath, vres_name, pres_name, uv2_name)

            bmesh.vertices.add(len(dataMesh.vertices))
            bmesh.vertices.foreach_set('co', unpack_list(dataMesh.vertices))

            # TODO:
            #bmesh.vertices.foreach_set('normal', unpack_list(dataMesh.normal_list))

            nbr_faces = len(dataMesh.indices)
            bmesh.polygons.add(nbr_faces)

            bmesh.polygons.foreach_set('loop_start', range(0, nbr_faces*3, 3))
            bmesh.polygons.foreach_set('loop_total', (3,)*nbr_faces)

            bmesh.loops.add(nbr_faces*3)
            bmesh.loops.foreach_set('vertex_index', unpack_list(dataMesh.indices))

            nbr_faces = len(bmesh.polygons)
            bmesh.polygons.foreach_set('use_smooth', [True]*nbr_faces)

            uv2_faces = None
            if uv2_name:
                if dataMesh.uv2_list:
                    uv2_faces = bmesh.uv_textures.new()
                    uv2_name = bpy.path.clean_name(uv2_name)
                    uv2_faces.name = uv2_name
                else:
                    uv2_name = ''

            if dataMesh.uv_list:
                uv_faces = bmesh.uv_textures.new()
                uv_faces.name = 'uv1'
                uv_faces.active = True

                uv_layer = bmesh.uv_layers['uv1'].data[:]
                uv2_layer = bmesh.uv_layers[uv2_name].data[:] if uv2_faces else None

                for poly in bmesh.polygons:
                    for li in poly.loop_indices:
                        vi = bmesh.loops[li].vertex_index
                        uv_layer[li].uv = dataMesh.uv_list[vi]
                        if uv2_name:
                            uv2_layer[li].uv = dataMesh.uv2_list[vi]
            else:
                av_PrintWarn('uv_faces is None')

            for primitiveGroup in renderSet.findall('geometry/primitiveGroup'):
                _identifier = primitiveGroup.find('material/identifier').text.strip()
                _index = av_AsInt(primitiveGroup.text)

                if primitiveGroup.find('material/fx') is not None:
                    _shader_name = os.path.basename(primitiveGroup.find('material/fx').text.strip())

                    material = bpy.data.materials.new(_identifier)
                    kill_list['Materials'].append(material.name)

                    bmesh.materials.append(material)

                    startIndex = dataMesh.PrimitiveGroups[_index]['startIndex']//3
                    endPrimitives = startIndex + dataMesh.PrimitiveGroups[_index]['nPrimitives']

                    for fidx, pl in enumerate(bmesh.polygons):
                        if startIndex <= fidx <= endPrimitives:
                            pl.material_index = _index

            bmesh.validate()
            bmesh.update()

            ob = bpy.data.objects.new(mesh_name, bmesh)
            kill_list['Objects'].append(ob.name)

            if model.get('is_child') and model.get('Position'):
                av_PrintInfo('is_child')
                # ob.scale = model['Scale']
                # ob.rotation_euler = model['Rotation']
                ob.location = model['Position']

            bpy.context.scene.objects.link(ob)

        remove_unp_file(unp_primitives_filepath)
        if model.get('is_root'):
            av_PrintInfo('is_root')
            return self.load_positions(visual_xml)



g_BigWorldModelLoader = BigWorldModelLoader()
