''' SkepticalFox 2015-2024 '''



__all__ = ('g_PackageManager',)



#####################################################################
# imports

import os

from zipfile import ZipFile
from bpy.app import tempdir


pkg_filename_list = (
	'vehicles_airplane_1_hd.pkg', 'vehicles_airplane_1.pkg',
	'vehicles_airplane_2_hd.pkg', 'vehicles_airplane_2.pkg',
	'vehicles_airplane_3_hd.pkg', 'vehicles_airplane_3.pkg',
	'vehicles_airplane_4_hd.pkg', 'vehicles_airplane_4.pkg',
	'vehicles_airplane_5_hd.pkg', 'vehicles_airplane_5.pkg',
	'vehicles_airplane_6_hd.pkg', 'vehicles_airplane_6.pkg',
	'vehicles_airplane_7_hd.pkg', 'vehicles_airplane_7.pkg',
	'vehicles_airplane_8_hd.pkg', 'vehicles_airplane_8.pkg',
	'vehicles_airplane_9_hd.pkg', 'vehicles_airplane_9.pkg',
	'vehicles_airplane_10_hd.pkg', 'vehicles_airplane_10.pkg',
	'shared_content_spaces_hd.pkg', 'shared_content_spaces.pkg',
	'shared_content_aircrafts_hd.pkg', 'shared_content_aircrafts.pkg',
	'00_01_hangar_base_hd.pkg', '00_01_hangar_base.pkg',
	'00_02_hangar_premium_hd.pkg', '00_02_hangar_premium.pkg'
)



#####################################################################
# PackageManager

class PackageManager:
    __pkg_zipfile_list = []
    __pkg_filepath_dict = {}
    __res_pkg_path = ''
    is_loaded = False

    def load(self, res_path):
        self.__res_pkg_path = res_path
        pkg_idx = 0
        for pkg in pkg_filename_list:
            pkg_abs_path = os.path.join(self.__res_pkg_path, pkg)
            if os.path.exists(pkg_abs_path):
                zfile = ZipFile(pkg_abs_path, 'r')
                self.__pkg_zipfile_list.append(zfile)
                for _file in zfile.infolist():
                    self.__pkg_filepath_dict[_file.filename.lower()] = (pkg_idx, _file.filename)
                pkg_idx += 1
        self.is_loaded = True

    def clear(self):
        self.__res_pkg_path = ''
        self.is_loaded = False
        self.__pkg_filepath_dict.clear()
        self.__pkg_zipfile_list.clear()

    def open_file(self, filename):
        filename = filename.lower()
        if self.__pkg_filepath_dict.get(filename):
            pkg_idx, file_path = self.__pkg_filepath_dict[filename]
            try:
                from io import BytesIO
                return BytesIO(self.__pkg_zipfile_list[pkg_idx].read(file_path))
            except:pass
        return None

    def extract_file(self, file2, file1=''):
        if file1:
            file1 = file1.lower()
            if self.__pkg_filepath_dict.get(file1):
                pkg_idx, file_path = self.__pkg_filepath_dict[file1]
                try:
                    return self.__pkg_zipfile_list[pkg_idx].extract(file_path, tempdir)
                except:pass
        file2 = file2.lower()
        if self.__pkg_filepath_dict.get(file2):
            pkg_idx, file_path = self.__pkg_filepath_dict[file2]
            try:
                return self.__pkg_zipfile_list[pkg_idx].extract(file_path, tempdir)
            except:pass
        return ''

g_PackageManager = PackageManager()
