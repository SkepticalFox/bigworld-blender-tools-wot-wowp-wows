''' SkepticalFox 2015-2024 '''

# original: https://github.com/konlil/mypy/blob/master/vertices.py



#####################################################################
# vt_baseType

class vt_baseType:
    uv2 = None

    FORMAT = ''
    SIZE = 0
    IS_SKINNED = False
    V_TYPE = ''

    def __init__(self, t):
        self.__tuple = t

    def __str__(self):
        return str(self.__tuple)



#####################################################################
# vt_XYZNUVIIIWWTB

class vt_XYZNUVIIIWWTB(vt_baseType):
    FORMAT = '<3fI2f5B2I'
    SIZE = 37
    IS_SKINNED = True
    V_TYPE = 'xyznuviiiwwtb'

    def __init__(self, t):
        super(vt_XYZNUVIIIWWTB, self).__init__(t)
        self.pos = (t[0], -t[2], t[1])
        self.normal = t[3]
        self.uv = (t[4], 1-t[5])
        self.index = t[6]
        self.index2 = t[7]
        self.index3 = t[8]
        self.weight = t[9]
        self.weight2 = t[10]
        self.tangent = t[11]
        self.binormal = t[12]

    def to_tuple(self):
        # if self.uv2 is not None:
        #     return (self.pos, self.normal, self.uv, self.uv2, self.index, self.index2, self.index3, self.weight, self.weight2, self.tangent, self.binormal)
        # return (self.pos, self.normal, self.uv, self.index, self.index2, self.index3, self.weight, self.weight2, self.tangent, self.binormal)
        if self.uv2 is not None:
            return (self.pos, self.normal, self.uv, self.uv2, self.tangent, self.binormal)
        return (self.pos, self.normal, self.uv, self.tangent, self.binormal)



#####################################################################
# vt_XYZNUVTB

class vt_XYZNUVTB(vt_baseType):
    FORMAT = '<3fI2f2I'
    SIZE = 32
    IS_SKINNED = False
    V_TYPE = 'xyznuvtb'

    def __init__(self, t):
        super(vt_XYZNUVTB, self).__init__(t)
        self.pos = (t[0], t[2], t[1])
        self.normal = t[3]
        self.uv = (t[4], 1-t[5])
        self.tangent = t[6]
        self.binormal = t[7]

    def to_tuple(self):
        if self.uv2 is not None:
            return (self.pos, self.normal, self.uv, self.uv2, self.tangent, self.binormal)
        return (self.pos, self.normal, self.uv, self.tangent, self.binormal)



#####################################################################
# vt_XYZNUV

class vt_XYZNUV(vt_baseType):
    FORMAT = '<8f'
    SIZE = 32
    IS_SKINNED = False
    IS_NEW = False
    V_TYPE = 'xyznuv'

    def __init__(self, t):
        super(vt_XYZNUV, self).__init__(t)
        self.pos = (t[0], t[2], t[1])
        self.normal = (t[3], t[4], t[5])
        self.uv = (t[6], 1-t[7])

    def to_tuple(self):
        if self.uv2 is not None:
            return (self.pos, self.normal, self.uv, self.uv2)
        return (self.pos, self.normal, self.uv)
