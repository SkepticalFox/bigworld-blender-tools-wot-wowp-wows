''' SkepticalFox 2015-2024 '''



#####################################################################
# imports

from mathutils import Vector
from ..common.XmlUnpacker import XmlUnpacker



#####################################################################
# globals

g_XmlUnpacker = XmlUnpacker()



#####################################################################
# functions

def av_AsVector(vector_str):
    return Vector(tuple(map(float, vector_str.strip().split())))



def av_AsBool(bool_str):
    if 'true' in bool_str.lower():
        return True
    return False



def av_AsInt(int_str):
    if int_str is None:
        av_PrintError('int_str is None')
    int_str = int_str.strip()
    if int_str.isdigit():
        return int(int_str)
    return 0



def av_AsFloat(float_str):
    return float(float_str)



def av_AsNormPath(path_str):
    return '/'.join(path_str.strip().split('\\'))



def av_Log(log_str):
    if True:
        print(log_str)
    else:
        with open('av_log.txt', 'a') as f:
            f.write(log_str)



def av_PrintInfo(info_str):
    av_Log('Info: %s' % info_str)



def av_PrintWarn(warn_str):
    av_Log('Warn: %s' % warn_str)



def av_PrintError(err_str):
    av_Log('Error: %s' % err_str)



def av_PrintSplitter():
    av_Log('='*12)
