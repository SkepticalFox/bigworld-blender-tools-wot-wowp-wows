''' SkepticalFox 2015-2024 '''



#####################################################################
# imports

from .common import *

from xml.etree.ElementTree import parse

import os
import gettext
import sqlite3



__all__ = ('g_AircraftUtils', 'g_LevelsL10n', 'g_NationsL10n', 'g_CategoryL10n')



#####################################################################
# AircraftListPaths

class AircraftListPaths:
    JAPAN = 'scripts\\db\\aircrafts\\Japan'
    CHINA = 'scripts\\db\\aircrafts\\China'
    FRANCE = 'scripts\\db\\aircrafts\\France'
    GB = 'scripts\\db\\aircrafts\\GB'
    GERMANY = 'scripts\\db\\aircrafts\\Germany'
    USA = 'scripts\\db\\aircrafts\\USA'
    USSR = 'scripts\\db\\aircrafts\\USSR'
    INDEX = {
        1: JAPAN, 2: CHINA,
        3: FRANCE, 4: GB,
        5: GERMANY, 6: USA,
        7: USSR
    }



#####################################################################
# MoFiles

class MoFiles:
    AIRPLANES_MO = 'localization\\text\\LC_MESSAGES\\airplanes.mo'
    AIRPLANES_ID = 'airplanes'
    LOBBY_MO = 'localization\\text\\LC_MESSAGES\\lobby.mo'
    LOBBY_ID = 'lobby'
    ALL = {
        AIRPLANES_ID: [AIRPLANES_MO, None],
        LOBBY_ID: [LOBBY_MO, None]
    }

    def load(self, wowp_res_path):
        for key, value in self.ALL.items():
            mo_path = os.path.join(wowp_res_path, value[0])
            if not os.path.exists(mo_path):
                continue
            self.ALL[key][1] = gettext.GNUTranslations(open(mo_path, 'rb'))

    def _l(self, str_):
        try:
            return self.ALL[self.LOBBY_ID][1].gettext(str_)
        except:
            return str_

    def _a(self, str_):
        try:
            return self.ALL[self.AIRPLANES_ID][1].gettext(
                'PLANE_NAME_%s' % str_.upper())
        except:
            return str_



#####################################################################
# g_MoFiles

g_MoFiles = MoFiles()



#####################################################################
# LevelListL10n

class LevelsL10n:
    _ALL = 'all'
    _FIRST = '1'
    _SECOND = '2'
    _THIRD = '3'
    _FOURTH = '4'
    _FIFTH = '5'
    _SIXTH = '6'
    _SEVENTH = '7'
    _EIGHTH = '8'
    _NINTH = '9'
    _TENTH = '10'
    ALL = {
        _ALL: 0, _FIRST: 1,
        _SECOND: 2, _THIRD: 3,
        _FOURTH: 4, _FIFTH: 5,
        _SIXTH: 6, _SEVENTH: 7,
        _EIGHTH: 8, _NINTH: 9,
        _TENTH: 10
    }
    INDEX = {}

    def load(self):
        for key, value in self.ALL.items():
            self.INDEX[value] = '%s' % key



#####################################################################
# g_LevelsL10n

g_LevelsL10n = LevelsL10n()



#####################################################################
# NationsL10n

class NationsL10n:
    _ALL_NATIONS = 'ALL_NATIONS'
    _JAPAN = 'JAPAN'
    _JAPAN_LOWER = 'japan'
    _CH = 'CH'
    _CHINA_LOWER = 'china'
    _FR = 'FR'
    _FRANCE_LOWER = 'france'
    _GB = 'GB'
    _GB_LOWER = 'gb'
    _GERMANY = 'GERMANY'
    _GERMANY_LOWER = 'germany'
    _USA = 'USA'
    _USA_LOWER = 'usa'
    _USSR = 'USSR'
    _USSR_LOWER = 'ussr'
    ALL = {
        _ALL_NATIONS: 0, _JAPAN: 1,
        _CH: 2, _FR: 3,
        _GB: 4, _GERMANY: 5,
        _USA: 6, _USSR: 7
    }
    ALL_LOWER = {
        _JAPAN_LOWER: 1, _CHINA_LOWER: 2,
        _FRANCE_LOWER: 3, _GB_LOWER: 4,
        _GERMANY_LOWER: 5, _USA_LOWER: 6,
        _USSR_LOWER: 7
    }
    INDEX = {}

    def load(self):
        for key, value in self.ALL.items():
            self.INDEX[value] = g_MoFiles._l('CAROUSEL_%s' % key)



#####################################################################
# g_NationsL10ns

g_NationsL10n = NationsL10n()



#####################################################################
# CategoryL10n

class CategoryL10n:
    _ALL_CLASSES = 'ALL_CLASSES'
    _ASSAULT = 'ASSAULT'
    _FIGHTERS = 'FIGHTERS'
    _HEAVY_FIGHTERS = 'HEAVY_FIGHTERS'
    _NAVY_FIGHTERS = 'NAVY_FIGHTERS'
    ALL = {
        _ALL_CLASSES: 0, _ASSAULT: 1,
        _FIGHTERS: 2, _HEAVY_FIGHTERS: 3,
        _NAVY_FIGHTERS: 4
    }
    INDEX = {}

    def load(self):
        for key, value in self.ALL.items():
            self.INDEX[value] = g_MoFiles._l('LOBBY_SHOP_LABEL_%s' % key)



#####################################################################
# g_CategoryL10n

g_CategoryL10n = CategoryL10n()



#####################################################################
# AircraftUtils

class AircraftUtils:
    __wowp_res_path = None
    __conn = None
    sqlite3_cursor = None

    def __init__(self):
        self.__conn = sqlite3.connect(':memory:')
        self.sqlite3_cursor = self.__conn.cursor()
        self.sqlite3_cursor.execute('CREATE TABLE AircraftList_table (id INTEGER PRIMARY KEY NOT NULL, nation_id INTEGER, type_id INTEGER, name TEXT, name_in_res TEXT, level INTEGER);')

    def sqlite3close(self):
        self.sqlite3_cursor.close()
        self.__conn.close()

    def setPath(self, wowp_res_path):
        self.__wowp_res_path = wowp_res_path

    def loadTranslations(self):
        g_MoFiles.load(self.__wowp_res_path)
        g_LevelsL10n.load()
        g_NationsL10n.load()
        g_CategoryL10n.load()

    def loadAircraftTable(self):
        aircrafts_xml_filepath = os.path.join(os.path.dirname(__file__), 'client_aircrafts\\aircrafts.xml')
        aircrafts_tree = parse(aircrafts_xml_filepath)
        aircrafts_elem = aircrafts_tree.getroot()
        for aircraft_elem in aircrafts_elem:
            aircraft_category = av_AsInt(aircraft_elem.find('type').text.strip())
            aircraft_name = aircraft_elem.find('name').text.strip()
            aircraft_level = av_AsInt(aircraft_elem.find('level').text.strip())
            aircraft_nation_idx	= g_NationsL10n.ALL_LOWER[aircraft_elem.find('country').text.strip().lower()]

            aircraft_xml = '%s.xml' % aircraft_name
            aircraft_l10n_name = g_MoFiles._a(aircraft_name)

            self.sqlite3_cursor.execute('INSERT INTO AircraftList_table(nation_id, type_id, name, name_in_res, level) VALUES (?, ?, ?, ?, ?);', (
                aircraft_nation_idx, aircraft_category, aircraft_l10n_name, aircraft_xml, aircraft_level))

    def loadInfo(self, xml_file, nation_idx):
        xml_path = os.path.join(self.__wowp_res_path, AircraftListPaths.INDEX[nation_idx], xml_file)

        with open(xml_path, 'rb') as f:
            element = g_XmlUnpacker.read(f)

        info = {}

        # Components -> Weapons2 -> slot -> type -> linkedModels -> visual -> parentUpgrade + model + mountPath
        # Airplane -> parts -> part -> upgrade -> states -> state -> model

        for part in element.findall('Airplane/parts/part'):
            name = part.find('name').text.strip()
            mountPoint = part.find('mountPoint').text.strip()
            for state in part.findall('upgrade/states/state'):
                info[name] = {
                    'File': state.find('model').text.strip(),
                    'mountPoint': mountPoint
                }
                break
            #break

        return info



#####################################################################
# g_AircraftUtils

g_AircraftUtils = AircraftUtils()
