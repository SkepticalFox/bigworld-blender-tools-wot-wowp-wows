''' SkepticalFox 2015-2024 '''



#####################################################################
# Addon - header

bl_info = {
    "name": "WoT servercollision (.xml)",
    "author": "SkepticalFox",
    "version": (0, 0, 2),
    "blender": (4, 3, 0),
    "location": "File > Import-Export",
    "description": "Test version",
    "warning": "Test version",
    "wiki_url": "https://www.koreanrandom.com/forum/topic/28240-/",
    "category": "Import-Export",
}



#####################################################################
# imports

import bpy

from bpy_extras.io_utils import ImportHelper

from .scimporter import g_ServerCollisionImporter



#####################################################################
# unregister

def unregister():
    bpy.types.INFO_MT_file_import.remove(menu_func_import)
    bpy.utils.unregister_class(Import_From_servercollision)



#####################################################################
# register

def register():
    bpy.types.INFO_MT_file_import.append(menu_func_import)
    bpy.utils.register_class(Import_From_servercollision)



#####################################################################
# menu_func

def menu_func_import(self, context):
    self.layout.operator('import_model.servercollision', text='WoT servercollision (.xml)')



#####################################################################
# Import_From_servercollision

class Import_From_servercollision(bpy.types.Operator, ImportHelper):
    bl_idname = 'import_model.servercollision'
    bl_label = 'Import servercollision'
    bl_description = 'Import WoT servercollision'
    bl_options = {'UNDO'}
    filename_ext = '*.xml'
    filter_glob = bpy.props.StringProperty(
        default = '*.xml',
        options = {'HIDDEN'}
    )

    def execute(self, context):
        g_ServerCollisionImporter.load(self.filepath)
        return {'FINISHED'}
