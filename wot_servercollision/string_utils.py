''' SkepticalFox 2015-2024 '''



#####################################################################
# functions

def AsVector(vector_str):
    from mathutils import Vector
    return Vector(tuple(map(float, vector_str.strip().split())))



def AsMatrix4x4T(vector_str):
    from mathutils import Matrix
    vector_16 = AsVector(vector_str)
    return Matrix(
        (vector_16[:4], vector_16[4:8], vector_16[8:12], vector_16[12:16])).transposed()



def AsBool(bool_str):
    if 'true' in bool_str.lower():
        return True
    return False



def AsInt(int_str):
    int_str = int_str.strip()
    if int_str.isdigit():
        return int(int_str)
    return 0



def AsFloat(float_str):
    return float(float_str)
