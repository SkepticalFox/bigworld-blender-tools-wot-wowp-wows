''' SkepticalFox 2015-2024 '''



#####################################################################
# imports

import os

import bpy

from mathutils import Vector

from .common import *



#####################################################################
# ServerCollisionImporter

class ServerCollisionImporter:
    @staticmethod
    def load(filename):
        with open(filename, 'rb') as f:
            servercollision = g_XmlUnpacker.read(f)

        bmesh = bpy.data.meshes.new('servercollision')

        # points
        points_quantity = shr_AsInt(servercollision.find('points').find('quantity').text)
        verts = []
        for point in servercollision.find('points').findall('point'):
            id = shr_AsInt(point.find('id').text)
            geometry = shr_AsVector(point.find('geometry').text)
            verts.append(geometry)

        # planes
        planes_quantity = shr_AsInt(servercollision.find('planes').find('quantity').text)
        faces = []
        for plane in servercollision.find('planes').findall('plane'):
            id = shr_AsInt(plane.find('id').text)
            pqty = shr_AsInt(plane.find('pqty').text)
            pindices = tuple(map(shr_AsInt, plane.find('pindices').text))
            normal = shr_AsVector(plane.find('normal').text)
            if pqty not in (3, 4, 6):
                shr_PrintWarn('pqty = %s' % pqty)
            faces.append(pindices)

        bmesh.from_pydata(verts, [], faces)

        bmesh.validate()
        bmesh.update()

        ob = bpy.data.objects.new('servercollision', bmesh)
        bpy.context.scene.objects.link(ob)



g_ServerCollisionImporter = ServerCollisionImporter()
