"""SkepticalFox 2015-2024"""

bl_info = {
    "name": "BigWorld Model (.primitives)",
    "author": "SkepticalFox",
    "version": (0, 1, 0),
    "blender": (4, 3, 0),
    "location": "File > Import-Export",
    "description": "BigWorld Model Import/Export plugin",
    "warning": "Test version",
    "wiki_url": "https://kr.cm/f/t/28240/",
    "category": "Import-Export",
}


# imports
import logging
import os
import traceback
from itertools import groupby
from pathlib import Path

# blender imports
import bpy  # type: ignore
from bpy_extras.io_utils import ExportHelper, ImportHelper  # type: ignore
from mathutils import Vector  # type: ignore

# local imports
from .common.consts import visual_property_descr_dict
from .export_bw_primitives import BigWorldModelExporter
from .export_bw_primitives_processed import BigWorldModelExporterProcessed
from .export_bw_primitives_skinned import BigWorldModelExporterSkinned
from .export_bw_primitives_skinned_processed import BigWorldModelExporterSkinnedProcessed
from .import_bw_primitives import load_bw_primitive_from_file
from .loadctree import ctree_load


logging.basicConfig()
logger = logging.getLogger(__name__)


def menu_func_import_ctree(self, context):
    self.layout.operator(Import_From_CtreeFile.bl_idname, text="BigWorld (.ctree)")


def menu_func_import(self, context):
    self.layout.operator(Import_From_ModelFile.bl_idname, text="BigWorld (.model)")


def menu_func_export(self, context):
    self.layout.operator(Export_ModelFile.bl_idname, text="BigWorld (.model)")


class BigWorld_Material_Panel(bpy.types.Panel):
    bl_label = "BigWorld Material"
    bl_idname = "MATERIAL_PT_bigworld_material"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_options = {"DEFAULT_CLOSED"}
    bl_context = "material"

    def draw(self, context):
        mat = context.material
        if not mat:
            return

        layout = self.layout

        layout.prop(mat, "BigWorld_Shader_Path")
        # layout.prop(mat, 'BigWorld_mfm_Path')

        layout.separator()
        for key, items in groupby(visual_property_descr_dict.items(), key=lambda it: it[1].type):
            layout.label(text=f"{key}:")
            for prop_name, _ in items:
                layout.prop(mat, f"BigWorld_{prop_name}")
            layout.separator()

        layout.prop(mat, "BigWorld_groupOrigin")


class Import_From_CtreeFile(bpy.types.Operator, ImportHelper):
    bl_idname = "import_model.ctree_model"
    bl_label = "Import Ctree Model"
    bl_description = "Import BigWorld Ctree Model"
    bl_options = {"UNDO"}

    filename_ext = ".ctree"
    filter_glob: bpy.props.StringProperty(
        default="*.ctree",
        options={"HIDDEN"},
    )  # type: ignore

    def execute(self, context):
        col = bpy.context.view_layer.active_layer_collection.collection
        ctree_load(col, Path(self.filepath))
        return {"FINISHED"}


class Import_From_ModelFile(bpy.types.Operator, ImportHelper):
    bl_idname = "import_model.bwmodel"
    bl_label = "Import Model"
    bl_description = "Import BigWorld Model"
    bl_options = {"UNDO"}

    filename_ext = ".model;.visual*;.primitives*"
    filter_glob: bpy.props.StringProperty(
        default="*.temp_model;*.model;*.visual*;*.primitives*",
        options={"HIDDEN"},
    )  # type: ignore

    import_empty: bpy.props.BoolProperty(
        name="Import Empty",
        description="",
        default=True,
    )  # type: ignore

    def execute(self, context):
        logger.info(f"import `{self.filepath}`")
        try:
            col = bpy.context.view_layer.active_layer_collection.collection
            load_bw_primitive_from_file(col, Path(self.filepath), self.import_empty)
        except Exception:
            self.report({"ERROR"}, f"Error in import `{self.filepath}`!")
            traceback.print_exc()
        return {"FINISHED"}

    def draw(self, context):
        layout = self.layout

        layout.prop(self, "import_empty")


def get_nodes_by_empty(obj, export_info, is_root=True):
    if is_root:
        node_name = "Scene Root"
    else:
        node_name = os.path.splitext(obj.name)[0]
    export_info[node_name] = {
        "loc": obj.location.xzy.to_tuple(),
        "scale": obj.scale.xzy.to_tuple(),
        "children": {},
    }
    obj_models = []
    for child in obj.children:
        if (child.data is None) and isinstance(child, bpy.types.Object):
            get_nodes_by_empty(child, export_info[node_name]["children"], False)
        elif isinstance(child.data, bpy.types.Mesh):
            obj_models.append(child)
    return obj_models


class Export_ModelFile(bpy.types.Operator, ExportHelper):
    bl_idname = "export_model.bwmodel"
    bl_label = "Export Model"
    bl_description = "Export BigWorld Model"

    filename_ext = ".temp_model"
    filter_glob: bpy.props.StringProperty(
        default="*.temp_model",
        options={"HIDDEN"},
    )  # type: ignore

    is_processed: bpy.props.BoolProperty(
        name="Processed",
        description="Only for WoT 0.9.12+",
        default=False,
    )  # type: ignore

    visual_type: bpy.props.EnumProperty(
        name="Visual type",
        description="",
        items=(
            ("STATIC", "Static", "No bones"),
            ("SKINNED", "Animated, skinned", "Bones, rigged"),
        ),
    )  # type: ignore

    @classmethod
    def poll(cls, context):
        sel_obj = context.selected_objects
        if len(sel_obj) == 1:
            return (sel_obj[0].data is None) and isinstance(sel_obj[0], bpy.types.Object) and sel_obj[0].children
        return False

    def get_export_object(self, obj_models):
        bpy.ops.object.select_all(action="DESELECT")
        tmp_ctx = bpy.context.copy()
        obs = []
        for obj_model in obj_models:
            new_obj = obj_model.copy()
            new_obj.data = new_obj.data.copy()
            obs.append(new_obj)
        logger.debug(obs)
        tmp_ctx["selected_objects"] = tmp_ctx["selected_editable_objects"] = obs
        tmp_ctx["object"] = tmp_ctx["active_object"] = tmp_ctx["selected_objects"][0]
        if len(tmp_ctx["selected_objects"]) > 1:
            bpy.ops.object.join(tmp_ctx)
        return tmp_ctx["selected_objects"][0]

    def execute(self, context):
        obj = context.selected_objects[0]
        export_info = {"nodes": {}}
        obj_models = get_nodes_by_empty(obj, export_info["nodes"])

        if len(obj_models):
            bb_min = Vector((10000.0, 10000.0, 10000.0))
            bb_max = Vector((-10000.0, -10000.0, -10000.0))

            export_info["exporter_version"] = "%s.%s.%s" % bl_info["version"]

            if self.visual_type == "STATIC":
                export_model = self.get_export_object(obj_models)

                export_model.data.transform(export_model.matrix_world)

                bb_min.x = min(export_model.bound_box[0][0], bb_min.x)
                bb_min.z = min(export_model.bound_box[0][1], bb_min.z)
                bb_min.y = min(export_model.bound_box[0][2], bb_min.y)

                bb_max.x = max(export_model.bound_box[6][0], bb_max.x)
                bb_max.z = max(export_model.bound_box[6][1], bb_max.z)
                bb_max.y = max(export_model.bound_box[6][2], bb_max.y)

                if self.is_processed:
                    bw_exporter = BigWorldModelExporterProcessed()
                else:
                    bw_exporter = BigWorldModelExporter()

                export_info["bb_min"] = bb_min.to_tuple()
                export_info["bb_max"] = bb_max.to_tuple()

                bw_exporter.export(export_model, self.filepath, export_info)

                bpy.data.objects.remove(export_model)

            elif self.visual_type == "SKINNED":
                for obj_model in obj_models:
                    if not obj_model.data.uv_layers:
                        self.report({"ERROR"}, "mesh.uv_layers is None")
                        logger.error("mesh.uv_layers is None")
                        return {"FINISHED"}

                    if not len(obj_model.data.materials):
                        # TODO:
                        # identifier = empty
                        # mfm = materials/template_mfms/lightonly.mfm
                        self.report({"ERROR"}, "mesh.materials is None")
                        return {"FINISHED"}

                    bb_min.x = min(obj_model.location.x + obj_model.bound_box[0][0], bb_min.x)
                    bb_min.z = min(obj_model.location.y + obj_model.bound_box[0][1], bb_min.z)
                    bb_min.y = min(obj_model.location.z + obj_model.bound_box[0][2], bb_min.y)

                    bb_max.x = max(obj_model.location.x + obj_model.bound_box[6][0], bb_max.x)
                    bb_max.z = max(obj_model.location.y + obj_model.bound_box[6][1], bb_max.z)
                    bb_max.y = max(obj_model.location.z + obj_model.bound_box[6][2], bb_max.y)

                if self.is_processed:
                    bw_exporter = BigWorldModelExporterSkinnedProcessed()
                else:
                    bw_exporter = BigWorldModelExporterSkinned()

                export_info["bb_min"] = bb_min.to_tuple()
                export_info["bb_max"] = bb_max.to_tuple()

                bw_exporter = BigWorldModelExporter()
                bw_exporter.export(obj_models, self.filepath, export_info)
        return {"FINISHED"}

    def draw(self, context):
        layout = self.layout
        layout.prop(self, "visual_type")
        layout.separator()
        layout.prop(self, "is_processed")


generic_register, generic_unregister = bpy.utils.register_classes_factory(
    (
        BigWorld_Material_Panel,
        Import_From_CtreeFile,
        Import_From_ModelFile,
        Export_ModelFile,
    )
)


def register():
    bpy.types.TOPBAR_MT_file_import.append(menu_func_import)
    bpy.types.TOPBAR_MT_file_import.append(menu_func_import_ctree)
    bpy.types.TOPBAR_MT_file_export.append(menu_func_export)

    bpy.types.Material.BigWorld_Shader_Path = bpy.props.StringProperty(
        name="fx",
        default="shaders/std_effects/lightonly.fx",
        description="DirectX Shader",
    )

    for name, desc in visual_property_descr_dict.items():
        setattr(
            bpy.types.Material,
            f"BigWorld_{name}",
            bpy.props.StringProperty(name=name, description=desc.description),
        )

    bpy.types.Material.BigWorld_groupOrigin = bpy.props.StringProperty(name="groupOrigin")

    generic_register()


def unregister():
    bpy.types.TOPBAR_MT_file_import.remove(menu_func_import)
    bpy.types.TOPBAR_MT_file_import.remove(menu_func_import_ctree)
    bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)
    generic_unregister()
