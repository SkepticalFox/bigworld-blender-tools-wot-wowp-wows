"""SkepticalFox 2015-2024"""

# imports
import logging
import os
import traceback
from pathlib import Path
from xml.etree import ElementTree as ET

# blender imports
import bpy  # type: ignore
from bpy_extras.io_utils import unpack_list  # type: ignore
from mathutils import Vector  # type: ignore

# local imports
from .common.XmlUnpacker import XmlUnpacker
from .common import utils_AsVector
from .common.consts import visual_property_descr_dict, VERBOSE_VALIDATE
from .loaddatamesh import LoadDataMesh


logger = logging.getLogger(__name__)


def get_empty_by_nodes(col: bpy.types.Collection, elem: ET.Element, empty_obj=None):
    if (elem.find("identifier") is None) or (elem.find("transform") is None):
        return None

    identifier = elem.findtext("identifier").strip()
    row0 = utils_AsVector(elem.findtext("transform/row0"))
    row1 = utils_AsVector(elem.findtext("transform/row1"))
    row2 = utils_AsVector(elem.findtext("transform/row2"))
    row3 = utils_AsVector(elem.findtext("transform/row3"))
    scale = Vector((row0.x, row2.z, row1.y))
    location = row3.xzy

    ob = bpy.data.objects.new(identifier, None)

    ob.scale = scale
    ob.location = location

    if empty_obj is not None:
        ob.parent = empty_obj

    col.objects.link(ob)

    for node in elem.iterfind("node"):
        get_empty_by_nodes(col, node, ob)

    return ob


def fake_visual_from_primitives(primitives_filepath: Path):
    root = ET.Element("root")

    # very hacky !!!
    dataMesh = LoadDataMesh(primitives_filepath)
    for name in dataMesh.packed_groups:
        if name.endswith("vertices"):
            renderSet_node = ET.SubElement(root, "renderSet")
            ET.SubElement(renderSet_node, "treatAsWorldSpaceObject").text = "false"
            geometry_node = ET.SubElement(renderSet_node, "geometry")
            ET.SubElement(geometry_node, "vertices").text = name
            ET.SubElement(geometry_node, "primitive").text = name.rpartition("vertices")[0] + "indices"
            uv2 = name.rpartition("vertices")[0] + "uv2"
            if uv2 in dataMesh.packed_groups:
                ET.SubElement(geometry_node, "stream").text = uv2

    return root


def load_bw_primitive_from_file(col: bpy.types.Collection, model_filepath: Path, import_empty: bool = False):
    visual_filepath = model_filepath.with_suffix(".visual_processed")
    visual_filepath_old = model_filepath.with_suffix(".visual")
    primitives_filepath = model_filepath.with_suffix(".primitives_processed")
    primitives_filepath_old = model_filepath.with_suffix(".primitives")

    is_processed = primitives_filepath.is_file()
    if not is_processed:
        visual_filepath = visual_filepath_old
        primitives_filepath = primitives_filepath_old

    has_visual = visual_filepath.is_file()
    if not primitives_filepath.is_file():
        raise Exception("There is no primitives file in the directory!")

    # cannot load empties w/o .visual
    if not has_visual:
        import_empty = False

    if has_visual:
        with visual_filepath.open("rb") as f:
            visual = XmlUnpacker().read(f)
    else:
        visual = fake_visual_from_primitives(primitives_filepath)

    if visual.find("renderSet") is None:
        return

    root_empty_ob = None
    for renderSet in visual.findall("renderSet"):
        vres_name = renderSet.findtext("geometry/vertices").strip()
        pres_name = renderSet.findtext("geometry/primitive").strip()
        mesh_name = os.path.splitext(vres_name)[0]
        bmesh = bpy.data.meshes.new(mesh_name)

        uv2_name = ""
        if renderSet.find("geometry/stream") is not None:
            stream_res_name = renderSet.findtext("geometry/stream").strip()
            if "uv2" in stream_res_name:
                uv2_name = stream_res_name

        logger.debug(primitives_filepath, vres_name, pres_name)
        dataMesh = LoadDataMesh(primitives_filepath, vres_name, pres_name, uv2_name)

        bmesh.vertices.add(len(dataMesh.vertices))
        bmesh.vertices.foreach_set("co", unpack_list(dataMesh.vertices))

        # TODO:
        # bmesh.vertices.foreach_set('normal', unpack_list(dataMesh.normal_list))

        nbr_faces = len(dataMesh.indices)
        bmesh.polygons.add(nbr_faces)

        bmesh.polygons.foreach_set("loop_start", range(0, nbr_faces * 3, 3))
        bmesh.polygons.foreach_set("loop_total", (3,) * nbr_faces)

        bmesh.loops.add(nbr_faces * 3)
        bmesh.loops.foreach_set("vertex_index", unpack_list(dataMesh.indices))

        nbr_faces = len(bmesh.polygons)
        bmesh.polygons.foreach_set("use_smooth", [True] * nbr_faces)

        uv2_layer = None
        if uv2_name:
            if dataMesh.uv2_list:
                uv2_layer = bmesh.uv_layers.new()
                uv2_layer.name = "uv2"
            else:
                uv2_name = ""

        if dataMesh.uv_list:
            uv_layer = bmesh.uv_layers.new()
            uv_layer.name = "uv1"
            uv_layer.active = True

            uv_layer = uv_layer.data[:]
            uv2_layer = uv2_layer.data[:] if uv2_layer else None

            for poly in bmesh.polygons:
                for li in poly.loop_indices:
                    vi = bmesh.loops[li].vertex_index
                    uv_layer[li].uv = dataMesh.uv_list[vi]
                    if uv2_name:
                        uv2_layer[li].uv = dataMesh.uv2_list[vi]
        else:
            logger.warning("Warn: uv_faces is None")

        primitiveGroupInfo = {}
        for primitiveGroup in renderSet.findall("geometry/primitiveGroup"):
            primitiveGroupInfo[int(primitiveGroup.text)] = {
                "identifier": primitiveGroup.findtext("material/identifier").strip(),
                "material_fx": primitiveGroup.findtext("material/fx"),
                "material_props": primitiveGroup.findall("material/property"),
                "groupOrigin": primitiveGroup.findtext("groupOrigin"),
            }

        for i, pg in enumerate(dataMesh.PrimitiveGroups):
            pgVisual = primitiveGroupInfo.get(i)
            material = bpy.data.materials.new(pgVisual["identifier"] if pgVisual else f"mat_{i}")
            bmesh.materials.append(material)

            startIndex = pg["startIndex"] // 3
            endPrimitives = startIndex + pg["nPrimitives"]

            for fidx, pl in enumerate(bmesh.polygons):
                if startIndex <= fidx <= endPrimitives:
                    pl.material_index = i

            if pgVisual:
                if primitiveGroupInfo[i]["material_fx"] is not None:
                    material.BigWorld_Shader_Path = pgVisual["material_fx"].strip()

                for property in pgVisual["material_props"]:
                    try:
                        property_name_xml = property.text.strip()
                        if property_name_xml in visual_property_descr_dict:
                            setattr(material, f"BigWorld_{property_name_xml}", property[0].text)
                    except Exception:
                        traceback.print_exc()

                if pgVisual["groupOrigin"] is not None:
                    material.BigWorld_groupOrigin = pgVisual["groupOrigin"].strip()

        logger.info("validating bmesh...")
        bmesh.validate(verbose=VERBOSE_VALIDATE)
        bmesh.update()

        ob = bpy.data.objects.new(mesh_name, bmesh)

        if "true" in renderSet.findtext("treatAsWorldSpaceObject"):
            if dataMesh.bones_info:
                bone_arr = []
                for node in renderSet.findall("node"):
                    bone_name = node.text.strip()
                    group = ob.vertex_groups.new(name=bone_name)
                    bone_arr.append({"name": bone_name, "group": group})

                for vert_idx, iiiww in enumerate(dataMesh.bones_info):
                    if len(iiiww) == 5:
                        bone_idx_vals = iiiww[0:3]
                        bone_wght_vals = iiiww[3:5]

                        index1 = bone_idx_vals[0] // 3
                        index2 = bone_idx_vals[1] // 3
                        index3 = bone_idx_vals[2] // 3

                        weight1 = bone_wght_vals[0] / 255.0
                        weight2 = bone_wght_vals[1] / 255.0
                        weight3 = 1 - weight1 - weight2

                        # types:
                        # 'ADD', 'REPLACE', 'SUBTRACT'
                        bone_arr[index1]["group"].add([vert_idx], weight1, "ADD")
                        bone_arr[index2]["group"].add([vert_idx], weight2, "ADD")
                        bone_arr[index3]["group"].add([vert_idx], weight3, "ADD")

                    elif len(iiiww) == 8:
                        bone_idx_vals = iiiww[0:3]
                        bone_wght_vals = iiiww[6:8]

                        index1 = bone_idx_vals[0] // 3
                        index2 = bone_idx_vals[1] // 3
                        index3 = bone_idx_vals[2] // 3

                        weight1 = bone_wght_vals[0] / 255.0
                        weight2 = bone_wght_vals[1] / 255.0
                        weight3 = 1 - weight1 - weight2

                        # types:
                        # 'ADD', 'REPLACE', 'SUBTRACT'
                        bone_arr[index1]["group"].add([vert_idx], weight3, "ADD")
                        bone_arr[index2]["group"].add([vert_idx], weight2, "ADD")
                        bone_arr[index3]["group"].add([vert_idx], weight1, "ADD")

        ob.scale = Vector((1.0, 1.0, 1.0))
        ob.rotation_euler = Vector((0.0, 0.0, 0.0))
        ob.location = Vector((0.0, 0.0, 0.0))

        if import_empty:
            if visual.find("node") is not None:
                if root_empty_ob is None:
                    root_empty_ob = get_empty_by_nodes(col, visual.findall("node")[0])
                if root_empty_ob is not None:
                    ob.parent = root_empty_ob

        col.objects.link(ob)
