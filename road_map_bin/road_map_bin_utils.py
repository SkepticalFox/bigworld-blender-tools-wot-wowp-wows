from struct import unpack, pack
from collections import OrderedDict
import json
import zlib
import io



class Unpacker:

	@staticmethod
	def read_str(bin):
		return unpack('256s', bin.read(256))[0].split(b'\x00')[0].decode('ascii')

	@classmethod
	def load(cls, bin):
		data = OrderedDict()
		data['decal_settings'] = {
			'texture_am': cls.read_str(bin),
			'texture_nm': cls.read_str(bin),
			'texture_gmm': cls.read_str(bin),
			'unknowns': unpack('<5If', bin.read(24)),
			'priority': unpack('b', bin.read(1))[0],
			'parallax_amplitude': unpack('<f', bin.read(4))[0],
			'parallax_offset': unpack('<f', bin.read(4))[0]
		}
		cnt = unpack('<I', bin.read(4))[0]
		data['other'] = []
		for i in range(cnt):
			data['other'].append(unpack('<29f', bin.read(29*4)))
		return data

	@classmethod
	def unpack(cls, bin_name, json_name):
		roads = []
		with open(bin_name, 'rb') as f:
			count = unpack('<I', f.read(4))[0]
			for i in range(count):
				size = unpack('<I', f.read(4))[0]
				data = f.read(size)
				assert data[:8] == b'\x00piz!\x90\xafB', data[:12]
				int_ = unpack('<I', data[8:12])[0]
				data = data[12:]
				unc_data = zlib.decompress(data)
				assert int_ == len(unc_data), (int_, len(unc_data))
				unc_data = io.BytesIO(unc_data)
				roads.append(cls.load(unc_data))
		with open(json_name, 'w') as fw:
			json.dump(roads, fw, indent='\t')



class Packer:

	@staticmethod
	def write_str(str):
		return pack('256s', str.encode('ascii'))

	@classmethod
	def pack_road(cls, d):
		ds = d['decal_settings']
		res = cls.write_str(ds['texture_am'])
		res += cls.write_str(ds['texture_nm'])
		res += cls.write_str(ds['texture_gmm'])
		res += pack('<5If', *ds['unknowns'])
		res += pack('b', ds['priority'])
		res += pack('<f', ds['parallax_amplitude'])
		res += pack('<f', ds['parallax_offset'])
		res += pack('<I', len(d['other']))
		for floats in d['other']:
			res += pack('<29f', *floats)
		return res

	@classmethod
	def pack(cls, json_name, bin_name):
		bin_data = b''
		with open(json_name, 'r') as f:
			unc_data = json.load(f)
		bin_data += pack('<I', len(unc_data))
		for road in unc_data:
			sub_bin_data = b'\x00piz!\x90\xafB'
			uncompressed = cls.pack_road(road)
			sub_bin_data += pack('<I', len(uncompressed))
			compressed = zlib.compress(uncompressed, 9)
			sub_bin_data += compressed
			bin_data += pack('<I', len(sub_bin_data))
			bin_data += sub_bin_data
		with open(bin_name, 'wb') as fw:
			fw.write(bin_data)



#Unpacker.unpack('road_map.bin', 'road_map.json')
#Packer.pack('road_map.json', 'new_road_map.bin')
#Unpacker.unpack('new_road_map.bin', 'new_road_map.json')
